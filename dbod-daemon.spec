#
# Database On Demand (DBOD) Daemon SPEC file
#

%define version 2.3

# Trying to avoid Koji post-generation issues
%define __arch_install_post %{nil} # /usr/lib/rpm/check-buildroot
%define debug_package %{nil} # Disables building debug RPM

Summary: DB On Demand Job Dispatching Daemon 
Name: cerndb-sw-dbod-daemon
Version: %{version}
Release: 2%{?dist} 
License: GPL
Group: Applications
ExclusiveArch: x86_64
Source: dbod_daemon-%{version}.tar.gz
URL: https://cern.ch/DBOnDemand/
Distribution: DBOD
Vendor: CERN
Packager: Ignacio Coterillo Coz <icoteril@cern.ch>

# Build requirements
BuildRequires: perl-Module-Install

# Requirements
Requires: perl-File-ShareDir
Requires: perl-Log-Log4perl
Requires: perl-YAML-Syck 
Requires: perl-DBD-Oracle
Requires: perl-Proc-Daemon >= 0.14

%description
DB On Demand Job Dispatching Daemon

%prep
%setup -n dbod_daemon-%{version}
exit 0

%build
perl Makefile.PL PREFIX=$RPM_BUILD_ROOT/usr/local
make
exit 0

%install
make install
# Additional folder structure
mkdir -p $RPM_BUILD_ROOT/etc/init.d/
mkdir -p $RPM_BUILD_ROOT/var/log/dbod_daemon
# Startup Scripts
cp scripts/dbod_daemon_init $RPM_BUILD_ROOT/etc/init.d/dbod-daemon
exit 0

%clean
rm -rf $RPM_BUILD_ROOT
exit 0

%post
/sbin/chkconfig --add dbod-daemon
exit 0

%preun
#only on uninstall, not on upgrades.
/etc/init.d/dbod-daemon stop  > /dev/null 2>&1
/sbin/chkconfig --del dbod-daemon

%files
/usr/local/share/perl5/DBOD.pm
%config /usr/local/share/perl5/auto/share/dist/dbod_daemon/dbod_daemon_logger.conf
/usr/local/share/perl5/auto/share/dist/dbod_daemon/dbod_daemon.conf-template
/usr/local/share/perl5/DBOD/Config.pm
/usr/local/share/perl5/DBOD/MySQL.pm
/usr/local/share/perl5/DBOD/PostgreSQL.pm
/usr/local/share/perl5/DBOD/LDAP.pm
/usr/local/share/perl5/DBOD/Middleware.pm
/usr/local/share/perl5/DBOD/Command.pm
/usr/local/share/perl5/DBOD/Database.pm
/usr/local/share/perl5/DBOD/Oracle.pm
/usr/local/share/perl5/DBOD/All.pm
%doc /usr/local/share/man/man1/dbod_daemon_monitor.1
%doc /usr/local/share/man/man1/dbod_state_checker.1
%doc /usr/local/share/man/man1/dbod_daemon.1
/usr/local/bin/dbod_state_checker
/usr/local/bin/dbod_daemon
/usr/local/bin/dbod_daemon_monitor
/etc/init.d/dbod-daemon
%attr (-, dbod, dbod) /var/log/dbod_daemon
/usr/local/lib64/perl5/auto/dbod_daemon/.packlist
/usr/local/lib64/perl5/perllocal.pod

%changelog
* Tue Oct 2 2018 Ignacio Coterillo <icoteril@cern.ch> 2.3.2
- Remove legacy copy and fix kscp commandline
- Remove /var/run definition (doesn't make sense on SystemD systems)
* Tue Apr 25 2017 Ignacio Coterillo <icoteril@cern.ch> 2.3.1
- Revert linter changes
* Fri Feb 3 2017 Ignacio Coterillo <icoteril@cern.ch> 2.2.3
- Change service name. Remove hardcoded path to executable in init script
* Thu Feb 2 2017 Ignacio Coterillo <icoteril@cern.ch> 2.2.2
- Packaging for CC7, Influx Support
* Tue Nov 4 2014 Ignacio Coterillo <icoteril@cern.ch> 2.2.1
- Added PostgreSQL config reload support and config copying on CERN new infra
* Thu Jun 19 2014 Ignacio Coterillo <icoteril@cern.ch> 2.1.1
- Updated list of files for AI infrastructure machines
* Thu Feb 27 2014 Ignacio Coterillo <icoteril@cern.ch> 2.1 
- Added Oracle 12c Support, Removed configuration templates
* Wed Sep 18 2013 Ignacio Coterillo <icoteril@cern.ch> 2.0
- Added Middleware, PostgreSQL support and dbod_daemon_monitor
* Mon Apr 15 2013 Ignacio Coterillo <icoteril@cern.ch> 1.6
- Initial packaging
