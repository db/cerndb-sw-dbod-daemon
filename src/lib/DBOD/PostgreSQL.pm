package DBOD::PostgreSQL;

use strict;
use warnings;
use Exporter;

use POSIX qw(strftime);

use DBOD::Config qw( $config );
use DBOD::Database;
use DBOD::All;
use DBOD::LDAP;

our ($VERSION, @ISA, @EXPORT_OK, $logger,);

$VERSION     = 2.2;
@ISA         = qw(Exporter);
@EXPORT_OK   = qw();

# Load general configuration

INIT{
    $logger = Log::Log4perl::get_logger( 'DBOD.PostgreSQL' );
    $logger->debug( "Logger created" );
} # INIT BLOCK

sub upgrade_callback{
    my ($job, $dbh) = @_;
    my $entity = DBOD::All::get_entity($job);
    my $version;
    my $params = $job->{'PARAMS'};
    foreach (@{$params}){
        if ($_->{'NAME'} =~ /VERSION_TO/){
            $version = $_->{'VALUE'};
            }
        }
    eval{
        $logger->debug( "Updating $entity version to $version in DB");
        DBOD::Database::update_instance($job, 'VERSION', $version, $dbh);
        1;
    } or do {
        $logger->error( "A problem occured updating $entity version on the DB");
        return;
    };
    eval {
        $logger->debug( "Updating $entity version to $version in LDAP");
        my $date = strftime "%H:%M:%S %m/%d/%Y", localtime;
        DBOD::LDAP::update_entity($entity, 
            [['SC-VERSION', $version],
             ['SC-COMMENT', "Upgraded to $version on $date"],
             ['SC-BINDIR-LOCATION', "/usr/local/pgsql/pgsql-$version"]]);
        1;
    } or do {
        $logger->error( "A problem occured updating $entity on LDAP");
        return;
    };

    return;
}

1;
