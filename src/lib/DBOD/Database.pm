package DBOD::Database;

use strict;
use warnings;
use Exporter;

use DBI;
use DBD::Oracle qw(:ora_types);
use POSIX qw(strftime);

use DBOD::Config qw( $config );

our ($VERSION, @ISA, @EXPORT_OK, $logger,
    $DSN, $DBTAG, $DATEFORMAT, $user, $password, $JOB_MAX_DURATION,
    $JOB_MAX_PENDING);

$VERSION     = 2.2;
@ISA         = qw(Exporter);
@EXPORT_OK   = qw( );

# Load general configuration

INIT{
    $logger = Log::Log4perl::get_logger( 'DBOD.Database' );
    $logger->debug( "Logger created" );
    # DB configuration parameters
    $DSN = $config->{'DB_DSN'} ;
    $DATEFORMAT = $config->{'DB_DATE_FORMAT'};
    $DBTAG = $config->{'DB_TAG'};
    $user = $config->{'DB_USER'};
    $password = $config->{'DB_PASSWORD'};
    $JOB_MAX_DURATION = $config->{'JOB_MAX_DURATION'};
    $JOB_MAX_PENDING = $config->{'JOB_MAX_PENDING'};
} # INIT BLOCK

sub get_instances_list{
    my $dbh = shift;
    my @result;
    eval {
        my $sql = "select username, db_name, db_type as type, state from dod_instances where status='1'";
        $logger->debug( $sql );
        my $sth = $dbh->prepare( $sql );
        $logger->debug("Executing statement");
        $sth->execute();
        $logger->debug( "[Instances]" );
        while ( my $ref = $sth->fetchrow_hashref() ){
            push @result, $ref;
            foreach my $key ( keys(%{$ref}) ) {
                my %h = %{$ref};
                $logger->debug( $key, "->", $h{$key} );
                }
            }
        $logger->debug( "Finishing statement" );
        $sth->finish();
        1;
    } or do{
        $logger->error( "Error fetching instances List : $DBI::errstr" );
        return ();
    };
    return @result;
}   

sub get_job_list{
    my $dbh = shift;
    my @result;
    eval {
        my $sql = "select a.username, a.db_name, a.command_name, a.type, a.creation_date
           from dod_jobs a inner join (
               select username, db_name, min(creation_date) as creation_date
               from dod_jobs
               where state = 'PENDING'
               group by username, db_name) b
           on a.username = b.username
           and a.db_name = b.db_name
           and a.creation_date = b.creation_date
           and (select count(*) 
                from dod_jobs c 
                where c.username = a.username 
                and c.db_name = a.db_name 
                and state = 'RUNNING') = 0
           where a.state = 'PENDING'";
        $logger->debug( $sql );
        my $sth = $dbh->prepare( $sql );
        $logger->debug("Executing statement");
        $sth->execute();
        while ( my $ref = $sth->fetchrow_hashref() ){
            push @result, $ref;
            $logger->debug( "[New Job]");
            foreach my $key ( keys(%{$ref}) ) {
                my %h = %{$ref};
                $logger->debug( $key, "->", $h{$key} );
                }
            }
        $logger->debug( "Finishing statement" );
        $sth->finish();
        1;
    } or do{
        $logger->error( "Error fetching job list : $DBI::errstr" );
        return ();
    };
    return @result;
}   

sub get_timed_out_jobs {
    my $dbh = shift;
    my @result;
    eval {
        my $sql = "select username, db_name, command_name, type, creation_date
            from dod_jobs where (state = 'RUNNING' or state = 'PENDING')
            and creation_date < (select sysdate from dual) -$JOB_MAX_DURATION/24"; 
        $logger->debug( $sql );
        my $sth = $dbh->prepare( $sql );
        $logger->debug("Executing statement");
        $sth->execute();
        while ( my $ref = $sth->fetchrow_hashref() ){
            push @result, $ref;
            $logger->debug( "[Timeout Job]");
            foreach my $key ( keys(%{$ref}) ) {
                my %h = %{$ref};
                $logger->debug( $key, "->", $h{$key} );
                }
            }
        $logger->debug( "Finishing statement" );
        $sth->finish();
        1;
    } or do {
        $logger->error( "Unable to check for TIMED OUT jobs : $DBI::errstr" );

        return (); # Returns an empty array in an attempt to fail smoothly
    };
    return @result;
}   

sub get_pending_jobs {
    my $dbh = shift;
    my $sql = "select count(*) from dod_jobs where state='PENDING' and sysdate - creation_date > ( $JOB_MAX_PENDING /86400 )";
    my $count;
    eval{
        ($count) = $dbh->selectrow_array($sql);
    } or do {
        $logger->error( "Unable to check for PENDING jobs : $DBI::errstr" );
        return;
    };
    return $count;
}

sub update_instance{
    my ($job, $col_name, $col_value, $dbh) = @_; 
    my $rc;
    eval {
        my $sql = "update DOD_INSTANCES set $col_name = ?
        where username = ? and db_name = ?";
        $logger->debug( "Preparing statement: $sql" );
        my $sth = $dbh->prepare( $sql );
        $logger->debug( "Binding parameters");
        $sth->bind_param(1, $col_value);
        $sth->bind_param(2, $job->{'USERNAME'});
        $sth->bind_param(3, $job->{'DB_NAME'});
        $logger->debug("Executing statement");
        $sth->execute();
        $logger->debug( "Finishing statement" );
        $sth->finish();
        $rc = 1;
        1;
    } or do {
        $logger->error( "Unable update instance status : $DBI::errstr" );
    };

    return $rc;
}


sub update_job{
    my ($job, $col_name, $col_value, $dbh) = @_;
    my $rc;
    eval {
        my $sth;
        my $sql;
        if ($col_name eq 'COMPLETION_DATE'){
            $sql = "update DOD_JOBS set $col_name = sysdate
            where username = ? and db_name = ? and command_name = ? and type = ? and creation_date = ?";
            $logger->debug( "Preparing statement: $sql" );
            $sth = $dbh->prepare( $sql );
            $logger->debug( "Binding parameters");
            $sth->bind_param(1, $job->{'USERNAME'});
            $sth->bind_param(2, $job->{'DB_NAME'});
            $sth->bind_param(3, $job->{'COMMAND_NAME'});
            $sth->bind_param(4, $job->{'TYPE'});
            $sth->bind_param(5, $job->{'CREATION_DATE'});
        }
        else{
            $sql = "update DOD_JOBS set $col_name = ?
            where username = ? and db_name = ? and command_name = ? and type = ? and creation_date = ?";
            $logger->debug( "Preparing statement: $sql" );
            $sth = $dbh->prepare( $sql );
            $logger->debug( "Binding parameters");
            $sth->bind_param(1, $col_value);
            $sth->bind_param(2, $job->{'USERNAME'});
            $sth->bind_param(3, $job->{'DB_NAME'});
            $sth->bind_param(4, $job->{'COMMAND_NAME'});
            $sth->bind_param(5, $job->{'TYPE'});
            $sth->bind_param(6, $job->{'CREATION_DATE'});
        }
        $logger->debug("Executing statement");
        $sth->execute();
        $logger->debug( "Finishing statement" );
        $sth->finish();
        $rc = 1;
        1;
    } or do {
        $logger->error( "Unable to update job status : $DBI::errstr" );
    };

    return $rc;
}

sub finish_job{
    my ($job, $resultCode, $log, $dbh, $no_callback) = @_;
    my $rc;
    eval{
        my $state_checker = DBOD::get_state_checker($job);
        if (!defined $state_checker){
            $logger->error( "Not state checker defined for this DB type" );
        }
        my ($job_state, $instance_state) = $state_checker->($job, $resultCode, 1);
        $logger->debug( "Updating job Completion Date" );
        update_job($job, 'COMPLETION_DATE', 'sysdate', $dbh);
        $logger->debug( "Updating job LOG" );
        update_job($job, 'LOG', $log, $dbh);
        $logger->debug( "Updating job STATE" );
        update_job($job, 'STATE', $job_state, $dbh);
        $logger->debug( "Updating Instance State" );
        update_instance($job, 'STATE', $instance_state, $dbh);
        unless (defined $no_callback) {
            my $callback = DBOD::get_callback($job);
            if (defined $callback) {
                $logger->debug( "Executing callback" );
                $callback->($job, $dbh);
            }
        }
        $rc = 1;
        1;
    } or do {
        $logger->error( "An error occured trying to finish the job : $DBI::errstr");
    };
    
    return $rc;
}

sub get_job_params{
    my ($job, $dbh) = @_;
    my $res;
    eval{
        $dbh->{LongReadLen} = 32768; 
        my $sql = "select NAME, VALUE from DOD_COMMAND_PARAMS
        where USERNAME = ? and DB_NAME = ? and COMMAND_NAME = ? and TYPE = ? and CREATION_DATE = ?";
        $logger->debug( "Preparing statement: $sql" );
        my $sth = $dbh->prepare( $sql, { ora_pers_lob => 1 } );
        $logger->debug( "Binding parameters");
        $sth->bind_param(1, $job->{'USERNAME'});
        $sth->bind_param(2, $job->{'DB_NAME'});
        $sth->bind_param(3, $job->{'COMMAND_NAME'});
        $sth->bind_param(4, $job->{'TYPE'});
        $sth->bind_param(5, $job->{'CREATION_DATE'});
        $logger->debug("Executing statement");
        $sth->execute();
        my @result;
        while ( my $ref = $sth->fetchrow_hashref() ){
            push @result, $ref;
        }
        $res = \@result;
        if ($sth->rows == 0){
            $res = undef;
        }
        $logger->debug( "Finishing statement" );
        $sth->finish();
        1;
    } or do {
        $logger->error( "Unable to fetch job parameters : $DBI::errstr" );
        return;
    };
    return $res; 
}

sub get_execution_string{
    my ($job, $dbh ) = @_;
    my $ref;
    eval {
        my $sql = "select EXEC from DOD_COMMAND_DEFINITION
        where COMMAND_NAME = ? and TYPE = ?";
        $logger->debug( "Preparing statement: $sql" );
        my $sth = $dbh->prepare( $sql );
        $logger->debug( "Binding parameters");
        $sth->bind_param(1, $job->{'COMMAND_NAME'});
        $sth->bind_param(2, $job->{'TYPE'});
        $logger->debug("Executing statement");
        $sth->execute();
        $ref = $sth->fetchrow_hashref();
        $logger->debug( "Finishing statement" );
        $sth->finish();
        1;
    } or do {
        $logger->error( "Unable to fetch command execution string : $DBI::errstr" );
        return;
    };
    return $ref->{'EXEC'}; 
}

sub get_db_handler{
    my $dbh;
    eval {
        $logger->debug( "Opening connection to $DSN" );
        $dbh = DBI->connect( $DSN, $user, $password, 
            { AutoCommit => 1, ora_client_info => 'dbod_daemon', ora_verbose => 0 });
        $logger->debug( "Setting date format: $DATEFORMAT" );
        $dbh->do( "alter session set NLS_DATE_FORMAT='$DATEFORMAT'" );
    } or do {
        $logger->error("Unable to obtain DB handler.\n Interpreter($@)\n libc($!)\n OS($^E)\nDBI($DBI::errstr)\n");
        $dbh = undef;
    };
    return $dbh;
}

# End of Module
END{

}

1;
