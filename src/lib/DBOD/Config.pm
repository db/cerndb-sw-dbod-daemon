package DBOD::Config;

use strict;
use warnings;
use Exporter;

use YAML::Syck;
use File::ShareDir;
use Log::Log4perl;

our ($VERSION, @ISA, @EXPORT_OK, $config, %cfg, $logger_cfg);

$VERSION     = 2.2;
@ISA         = qw(Exporter);
@EXPORT_OK   = qw( $config );

my $config_dir = File::ShareDir::dist_dir('dbod_daemon');
$config = LoadFile( "$config_dir/dbod_daemon.conf" );
$logger_cfg = "$config_dir/$config->{'LOGGER_CONFIG'}";

%cfg = %{$config};

Log::Log4perl::init( $logger_cfg );

my $logger = Log::Log4perl::get_logger( 'DBOD.Config' );
$logger->debug( "Logger created" );
$logger->debug( "Loaded configuration from $config_dir" );
foreach my $key ( keys(%{$config}) ) {
    my %h = %{$config};
    if ($key =~ /PASSWORD/) {
        $logger->debug( "\t$key -> XXXXXXXXX" );
    }
    else{
        $logger->debug( "\t$key -> $h{$key}" );
    }
}

1;
