use strict;
use warnings;
use Test::More qw(no_plan);;
use DBI;
use DBD::Oracle qw(:ora_types);
use POSIX qw(strftime);
use DBOD::Config qw( $config );
use Exporter;
use Scalar::Util qw(reftype);
use_ok('DBOD');
use_ok('DBOD::All');
use_ok('DBOD::Command');
use_ok('DBOD::Config');
use_ok('DBOD::Database');
use_ok('DBOD::LDAP');
use_ok('DBOD::Middleware');
use_ok('DBOD::MySQL');
use_ok('DBOD::Oracle');
use_ok('DBOD::PostgreSQL');

use DBOD::Config qw( $config );
use DBOD::Database;
use DBOD::MySQL;
use DBOD::Oracle;
use DBOD::PostgreSQL;
use DBOD::Middleware;
use DBOD::All;
use DBOD::Command;
#use Test::MockDBI;
use 5.010;


#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.


our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS, $logger,
    $DSN, $DBTAG, $DATEFORMAT, $user, $password, %callback_table);

$VERSION     = 2.1;
@ISA         = qw(Exporter);
@EXPORT      = qw(job_dispatcher $logger);
@EXPORT_OK   = ( );
%EXPORT_TAGS = ( );

# Load general configuration

INIT {
    $logger = Log::Log4perl::get_logger('DBOD');
    $logger->info('Logger created');
} # BEGIN BLOCK


my $username = 'lurodrig';
my $entity_name = 'lurodrig-1-beta.web.cern.ch';
my $command_name = 'echo';
my $type = 'TEST';
my $requester = 'thest';
my $admin_action = 1;


#getting db handler
my $dbh = DBOD::Database::get_db_handler();

#inserting testing job and its params to the db
my $state = 'PENDING';
my $stmt = 'INSERT INTO DOD_JOBS (USERNAME,DB_NAME,COMMAND_NAME,TYPE,CREATION_DATE,REQUESTER,
	ADMIN_ACTION,STATE) VALUES (?,?,?,?,SYSDATE,?,?,?)';
my $sth = $dbh->prepare($stmt)
	or die "Impossible" . $dbh->errstr;
$sth->execute($username,$entity_name,$command_name,$type,$requester,$admin_action,$state)
        or die "Impossible d'exécuter la requette: " . $sth->errstr;


my $dth = $dbh->prepare("INSERT INTO DOD_COMMAND_PARAMS (USERNAME, DB_NAME, COMMAND_NAME, TYPE, CREATION_DATE, 
	NAME, VALUE) VALUES (?,?,?,?, SYSDATE, 'TEXT', ?)")
	or die "Impossible sec: " . $dbh->errstr;

$dth->execute($username,$entity_name,,$command_name,$type,"1313")
        or die "Impossible sec d'exécuter la requette: " . $dth->errstr;

my @job_list = DBOD::Database::get_job_list($dbh);

my $jj = \@job_list;
if( $jj != 1)
{
	is(reftype $jj,'ARRAY',"Joblist is a defined array");
}

my $job = $job_list[-1];
$job->{'PARAMS'} = DBOD::Database::get_job_params($job, $dbh);
ok(reftype $job->{'PARAMS'} eq 'ARRAY', "Job pararm returned as arraylist");

my $upd = DBOD::Database::update_job($job, 'STATE', 'RUNNING', $dbh); 

my $exe_string = DBOD::Database::get_execution_string($job, $dbh);
ok($exe_string eq "-text :TEXT", "executute string ok");

my $entity = DBOD::All::get_entity($job);
ok($entity eq "dod_".$entity_name, "Entity name returned correctly");

my $cmd_line = DBOD::Command::prepare_command($job, $dbh);
print $cmd_line."\n";
ok($cmd_line eq "TEST_echo -text :TEXT", "CMD line constructed ok");

#DBOD::worker_body($job, $dbh);


#deleteting test command and paramaters from db
$dth = $dbh->prepare("DELETE FROM DOD_COMMAND_PARAMS WHERE TYPE = 'TEST'")
  
      or die "Impossible sec: " . $dbh->errstr;
$dth->execute()
        or die "Impossible sec d'exécuter la requette: " . $dth->errstr;

$dth = $dbh->prepare("DELETE FROM DOD_JOBS WHERE TYPE = 'TEST'")
        or die "Impossible sec: " . $dbh->errstr;
$dth->execute()
        or die "Impossible sec d'exécuter la requette: " . $dth->errstr;



