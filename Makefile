SPECFILE=$(shell find -maxdepth 1 -name \*.spec -exec basename {} \; )
REPOURL=git+ssh://git@gitlab.cern.ch:7999
# DB gitlab group
REPOPREFIX=/db

# Get all the package infos from the spec file
PKGVERSION=$(shell awk '/Version:/ { print $$2 }' ${SPECFILE})
PKGRELEASE=$(shell awk '/Release:/ { print $$2 }' ${SPECFILE} | sed -e 's/\%{?dist}//')
PKGNAME=$(shell awk '/Name:/ { print $$2 }' ${SPECFILE})
PKGID=$(PKGNAME)-$(PKGVERSION)
TARFILE=$(PKGID).tar.gz

compile:
	cd src && perl Makefile.PL && make

tar: compile
	cd src && make dist

# Installation as PERL Module
install-daemon: compile
	cd src && make install

# This is Koji required and must generate a suitable tarball

# The tar file needs to be in the repo as the Mock environment doesn't have
# perl-Module-Install available to build it.
sources:
	cp src/dbod_daemon-*.tar.gz .

# This task will generate an RPM locally
manual-rpm: tar  
	cp src/dbod_daemon-*.tar.gz $(SOURCESPATH)
	$(RPMBUILD) $(RPMFLAGS) dbod-daemon.spec

clean:
	rm -f dbod_daemon-*.tar.gz
	cd src && make clean

all: sources

srpm:   all
	rpmbuild -bs --define "_topdir ${PWD}" --define "_sourcedir $(PWD)" ${SPECFILE}

rpm:    all
	rpmbuild -ba --define "_topdir ${PWD}" --define "_sourcedir $(PWD)" ${SPECFILE}

scratch:
	koji build db7 --nowait --scratch  ${REPOURL}${REPOPREFIX}/${PKGNAME}.git#master

build:
	koji build db7 --nowait ${REPOURL}${REPOPREFIX}/${PKGNAME}.git#master

tag-qa:
	koji tag-build db7-qa $(PKGID)-$(PKGRELEASE).el7.cern

tag-stable:
	koji tag-build db7-stable $(PKGID)-$(PKGRELEASE).el7.cern
